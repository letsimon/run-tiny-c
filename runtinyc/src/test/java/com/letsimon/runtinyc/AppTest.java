package com.letsimon.runtinyc;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    public void testApp0()
    {
        assertTrue(App.run("/test/example0.c"));
    }
    
    public void testApp1()
    {
        assertTrue(App.run("/test/example1.c"));
    }
    
    public void testApp2()
    {
    	assertTrue(App.run("/test/example2.c"));
    }
    
    public void testApp3()
    {
    	assertTrue(App.run("/test/example3.c"));
    }
    
    public void testApp4()
    {
    	assertTrue(App.run("/test/example4.c"));
    }
    
    public void testApp5()
    {
        assertTrue(App.run("/test/example5.c"));
    }            
    
}
