package com.letsimon.runtinyc;

import java.io.IOException;
import java.io.InputStream;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;

import com.letsimon.runantlr.tinycLexer;
import com.letsimon.runantlr.tinycParser;
import com.letsimon.runtinyc.nodes.BaseNode;
import com.letsimon.runtinyc.nodes.ProgramNode;
import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.Truffle;

/**
 * Hello world!
 *
 */
public class App 
{
	public static boolean run(String file)
	{
		System.out.println("Reading resource " + file);
    	System.out.println("=== START ===");
    	long startTime = System.nanoTime();
        try {
        	InputStream inputStream = App.class.getResourceAsStream(file);
            Lexer lexer = new tinycLexer(CharStreams.fromStream(inputStream));
            
         	tinycParser parser = new tinycParser(new CommonTokenStream(lexer));
         	
         	Visitor visitor = new Visitor();
            
        	BaseNode node = visitor.visit(parser.program());
        	ProgramNode rootNode = new ProgramNode(node);
        	CallTarget callTarget = Truffle.getRuntime().createCallTarget(rootNode);
        	callTarget.call();	 
        } catch (IOException e) {
             e.printStackTrace();
        }
        long endTime = System.nanoTime();
        System.out.println("=== END ===");
        long timeElapsed = endTime - startTime;
        System.out.println("Execution time in milliseconds: " + timeElapsed / 1000000);
		return true;
	}
	
    public static void main( String[] args )
    {
    	if (args.length < 1) {
    		System.out.println("Error: Not enough arguments!");
    		System.out.println("USAGE: ./runtinyc RESOURCE");
    		System.out.println();
    		System.out.println("RESOURCE: example{0..5}.c");
    		System.exit(1);
    	}
    		
    	String file = "/test/" + args[0];
    	run(file);    	
    }
}
