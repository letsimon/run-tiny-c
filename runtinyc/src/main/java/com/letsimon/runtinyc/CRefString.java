package com.letsimon.runtinyc;

public class CRefString {
	public final String str;
	public int refCount;

	public CRefString(String str) {
		super();
		this.str = str;
		refCount = 0;
	}

	public int getRefCount() {
		return refCount;
	}

	public void incRefCount() {
		refCount++;
	}

	// returns true if refCount is zero
	public boolean decRefCount() {
		refCount--;
		return (refCount == 0);
	}

	public String getStr() {
		return str;
	} 
	
	
	
}
