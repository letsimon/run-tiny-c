package com.letsimon.runtinyc;

public class Utils {
	public static void panic(String msg) {
		System.out.println("PANIC: " + msg + " - exiting!");
		System.exit(42);
	}
}
