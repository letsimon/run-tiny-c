package com.letsimon.runtinyc;

import java.util.HashMap;
import java.util.Map;

import com.oracle.truffle.api.frame.FrameDescriptor;

public class TinycFrame {
	Map<String, Integer> intMap;
	Map<String, CRefString> strMap;
	
	public TinycFrame() {
		super();
		intMap = new HashMap<String, Integer>(); 
		strMap = new HashMap<String, CRefString>();
	}

	public FrameDescriptor getFrameDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setInt(String id, int val) {
		if (isString(id)) {
			CRefString tmp = strMap.get(id);
			if (tmp.decRefCount()) {
				// Deallocate tmp - Java GC takes care of that
			}
			strMap.remove(id);
		}
		intMap.put(id, val);
	}

	public boolean isInt(String id) {
		return intMap.containsKey(id);
	}
	
	public int getInt(String id) {
		if (isInt(id) == false) {
			Utils.panic("Variable " + id + " is undefined or not String");
		}
		return intMap.get(id);
	}

	public CRefString getString(String id) {
		if (isString(id) == false) {
			Utils.panic("Variable " + id + " is undefined or not String");
		}
		return strMap.get(id);
	}

	public boolean isString(String id) {
		return strMap.containsKey(id);
	}

	public void setString(String id, CRefString val) {
		if (isString(id)) {
			CRefString tmp = strMap.get(id);
			if (tmp.decRefCount()) {
				// Deallocate tmp - Java GC takes care of that
			}
		}
		if (isInt(id))
			intMap.remove(id);
		val.incRefCount();
		strMap.put(id, val);
	}
}
