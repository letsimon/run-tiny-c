package com.letsimon.runtinyc;

import java.util.ArrayList;
import java.util.List;

import com.letsimon.runantlr.tinycBaseVisitor;
import com.letsimon.runantlr.tinycParser.ExprContext;
import com.letsimon.runantlr.tinycParser.IdContext;
import com.letsimon.runantlr.tinycParser.IntegerContext;
import com.letsimon.runantlr.tinycParser.Paren_exprContext;
import com.letsimon.runantlr.tinycParser.ProgramContext;
import com.letsimon.runantlr.tinycParser.StatementContext;
import com.letsimon.runantlr.tinycParser.StringContext;
import com.letsimon.runantlr.tinycParser.SumContext;
import com.letsimon.runantlr.tinycParser.TermContext;
import com.letsimon.runantlr.tinycParser.TestContext;
import com.letsimon.runtinyc.nodes.AssignmentNode;
import com.letsimon.runtinyc.nodes.BaseNode;
import com.letsimon.runtinyc.nodes.ExprNode;
import com.letsimon.runtinyc.nodes.IfNode;
import com.letsimon.runtinyc.nodes.IntNode;
import com.letsimon.runtinyc.nodes.LessThanNode;
import com.letsimon.runtinyc.nodes.MultiStatementNode;
import com.letsimon.runtinyc.nodes.PrintNode;
import com.letsimon.runtinyc.nodes.ReadNode;
import com.letsimon.runtinyc.nodes.StatementNode;
import com.letsimon.runtinyc.nodes.StringNode;
import com.letsimon.runtinyc.nodes.SumNode;
import com.letsimon.runtinyc.nodes.SymbolNode;
import com.letsimon.runtinyc.nodes.WhileNode;

public class Visitor extends tinycBaseVisitor<BaseNode> {

	@Override
	public StatementNode visitProgram(ProgramContext ctx) {
		return visitStatement(ctx.statement()); 
	}

	@Override
	public StatementNode visitStatement(StatementContext ctx) {
		//int numTokens = ctx.children.size();
		String token0 = ctx.children.get(0).getText();
		if (token0.contentEquals("if")) {
			ExprNode cond = visitExpr(ctx.paren_expr().expr());
			StatementNode then = visitStatement(ctx.statement(0));
			StatementNode elseNode = (ctx.statement(1) == null) ? null : visitStatement(ctx.statement(1));
			return new IfNode(cond, then, elseNode);
		}
		if (token0.contentEquals("while")) {
			ExprNode cond = visitExpr(ctx.paren_expr().expr());
			if (cond == null)
				Utils.panic("While condition must not be empty (;)");
			StatementNode body = visitStatement(ctx.statement(0));
			if (body == null)
				Utils.panic("While body must not be empty (;)");
			return new WhileNode(cond, body);
		}
		if (token0.equals("print")) {
			ExprNode sum = visitExpr(ctx.expr());
			return new PrintNode(sum);
		}
		if (token0.equals("read")) {
			String id = getId(ctx.id());
			return new ReadNode(id);
		}
		if (token0.contentEquals("{")) {
			List<StatementNode> nodeList = new ArrayList<StatementNode>();
			for (StatementContext cc : ctx.statement()) {
				nodeList.add(visitStatement(cc));
			}
			return new MultiStatementNode(nodeList);
		}
		if (token0.equals(";")) {
			return null;
		}
		return visitExpr(ctx.expr());
	}

	@Override
	public BaseNode visitParen_expr(Paren_exprContext ctx) {
		// TODO Auto-generated method stub
		return super.visitParen_expr(ctx);
	}

	@Override
	public ExprNode visitExpr(ExprContext ctx) {
		if (ctx.test() != null) {
			return visitTest(ctx.test());
		}
		if (ctx.string() != null) {
			return new StringNode(getStringLiteral(ctx.string()));
		}
		String id = getId(ctx.id());
		ExprNode expr = visitExpr(ctx.expr());
		return new AssignmentNode(id, expr);
	}

	@Override
	public ExprNode visitTest(TestContext ctx) {
		ExprNode left = visitSum(ctx.sum(0));
		if (ctx.sum().size() == 1) {
			return left;
		}
		ExprNode right = visitSum(ctx.sum(1));
		//String token1 = ctx.children.get(1).getText();
		return new LessThanNode(left, right);
	}

	@Override	
	public ExprNode visitSum(SumContext ctx) {
		if (ctx.sum() == null) {
			return visitTerm(ctx.term());
		}
		ExprNode left = visitSum(ctx.sum());
		ExprNode right = visitTerm(ctx.term());
		String token1 = ctx.children.get(1).getText();
		boolean negative = token1.equals("-") ? true : false;
		return new SumNode(left, right, negative);
	}

	@Override
	public ExprNode visitTerm(TermContext ctx) {
		if (ctx.integer() != null) {
			int value = Integer.parseInt(ctx.integer().INT().getSymbol().getText());
			return new IntNode(value);
		}
		if (ctx.id() != null) {
			String id = getId(ctx.id());
			return new SymbolNode(id);
		}
		if (ctx.paren_expr() != null) {
			return visitExpr(ctx.paren_expr().expr());
		}
		System.out.println("visitTerm switch default");
		System.exit(3);
		return null;
	}

	// gets identifier value (as String) in compile time
	public String getId(IdContext ctx) {
		return ctx.STRING().getSymbol().getText();
	}
	
	// gets string literal value in compile time
	public String getStringLiteral(StringContext ctx) {
		String str = ctx.QSTRING().getSymbol().getText();
		return str.substring(1, str.length()-1);
	}
		
	@Override
	public BaseNode visitId(IdContext ctx) {
		return super.visitId(ctx);
		//return new IdNode(ctx.STRING().getSymbol().getText());
	}

	@Override
	public BaseNode visitInteger(IntegerContext ctx) {
		// TODO Auto-generated method stub
		return super.visitInteger(ctx);
	}

}
