package com.letsimon.runtinyc.nodes;

import java.util.List;

import com.letsimon.runtinyc.TinycFrame;

public class MultiStatementNode extends StatementNode {

	List<StatementNode> nodes;
	
	public MultiStatementNode(List<StatementNode> nodes) {
		super();
		this.nodes = nodes;
	}

	@Override
	public void executeVoid(TinycFrame frame) {
		for (StatementNode node : nodes) {
			if (node != null)
				node.executeVoid(frame);
		}
	}

}
