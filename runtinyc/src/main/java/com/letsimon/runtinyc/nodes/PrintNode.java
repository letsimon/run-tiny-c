package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.TinycFrame;
import com.letsimon.runtinyc.Utils;
import com.oracle.truffle.api.nodes.UnexpectedResultException;

public class PrintNode extends StatementNode {

	@Child ExprNode arg;

	public PrintNode(ExprNode arg) {
		super();
		this.arg = arg;
	}

	@Override
	public void executeVoid(TinycFrame frame) {
		try {
			System.out.println(arg.executeString(frame).getStr());
		} catch (UnexpectedResultException e) {
			try {
				System.out.println(Integer.toString(arg.executeInt(frame)));
			} catch (UnexpectedResultException e1) {
				Utils.panic("Print argument must return String or int");
			}
			
		}
	}

}
