package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.TinycFrame;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.RootNode;

public class ProgramNode extends RootNode {

	@Child BaseNode node;
	
	public ProgramNode(BaseNode node) {
		super(null);
		this.node = node;
	}

	@Override
	public Object execute(VirtualFrame frame) {
		TinycFrame newFrame = new TinycFrame();
		node.executeVoid(newFrame);
		return null;
	}
}
