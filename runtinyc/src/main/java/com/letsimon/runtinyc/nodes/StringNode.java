package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.CRefString;
import com.letsimon.runtinyc.TinycFrame;

public class StringNode extends ExprNode {

	final String value;
	
	public StringNode(String value) {
		super();
		this.value = value;
	}

	@Override
	public CRefString executeString(TinycFrame frame) {
		return new CRefString(value);
	}

	@Override
	public Object execute(TinycFrame frame) {
		return new CRefString(value);
	}

	
}
