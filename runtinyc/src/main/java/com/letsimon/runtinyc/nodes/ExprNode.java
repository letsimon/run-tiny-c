package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.CRefString;
import com.letsimon.runtinyc.TinycFrame;
import com.oracle.truffle.api.nodes.UnexpectedResultException;

public abstract class ExprNode extends StatementNode {
	
	public abstract Object execute(TinycFrame frame);
	
	public void executeVoid(TinycFrame frame) {
		this.execute(frame);
	}
	
	public int executeInt(TinycFrame frame) throws UnexpectedResultException {
	    Object obj = this.execute(frame);
	    if (obj instanceof Integer) {
	    	return (Integer) obj;
	    }
	    throw new UnexpectedResultException(obj);	    
	}
	
	public boolean executeBoolean(TinycFrame frame) throws UnexpectedResultException {
	    int res = this.executeInt(frame);
	    return (res != 0) ? true : false;
	}
		
	public CRefString executeString(TinycFrame frame) throws UnexpectedResultException {
		Object obj = this.execute(frame);
	    if (obj instanceof CRefString) {
	    	return (CRefString) obj;
	    }
	    throw new UnexpectedResultException(obj);	    
    }	
}
