package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.TinycFrame;

public class IntNode extends ExprNode {

	final int value;
	
	public IntNode(int value) {
		super();
		this.value = value;
	}

	@Override
	public int executeInt(TinycFrame frame) {
		return value;
	}
	
	@Override
	public Object execute(TinycFrame frame) {
		return value;
	}
}
