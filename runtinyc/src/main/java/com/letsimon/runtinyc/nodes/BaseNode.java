package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.TinycFrame;
import com.oracle.truffle.api.nodes.Node;

public abstract class BaseNode extends Node {
	
	public abstract void executeVoid(TinycFrame frame);
}