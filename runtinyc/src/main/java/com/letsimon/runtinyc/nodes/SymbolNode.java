package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.TinycFrame;
import com.letsimon.runtinyc.Utils;

public class SymbolNode extends ExprNode {
	
	final String id;
    
	public SymbolNode(String id) {
		super();
		this.id = id;
	}
	
	@Override
	public Object execute(TinycFrame frame) {
		if (frame.isInt(id))
			return frame.getInt(id);
		if (frame.isString(id))
			return frame.getString(id);
		Utils.panic("Variable '" + id + "' is undefined");
		return null;		
	}

}
