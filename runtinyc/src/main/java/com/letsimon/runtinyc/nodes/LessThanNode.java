package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.TinycFrame;
import com.letsimon.runtinyc.Utils;
import com.oracle.truffle.api.nodes.UnexpectedResultException;

public class LessThanNode extends ExprNode {

	@Child ExprNode left;
	@Child ExprNode right;
	
	public LessThanNode(ExprNode left, ExprNode right) {
		super();
		this.left = left;
		this.right = right;
	}
	
	@Override
	public Object execute(TinycFrame frame) {
		try {
			return (left.executeInt(frame) < right.executeInt(frame));
		} catch (UnexpectedResultException e) {
			Utils.panic("Test operands must return int");
		}
		return null;
	}

	@Override
	public boolean executeBoolean(TinycFrame frame) {
		try {
			return (left.executeInt(frame) < right.executeInt(frame));
		} catch (UnexpectedResultException e) {
			Utils.panic("Test operands must return int");
		}
		return false;
	}

	
}
