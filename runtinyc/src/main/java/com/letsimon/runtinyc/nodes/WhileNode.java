package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.TinycFrame;
import com.letsimon.runtinyc.Utils;
import com.oracle.truffle.api.nodes.UnexpectedResultException;

public class WhileNode extends StatementNode {

	@Child ExprNode cond;
	@Child StatementNode body;
	

	public WhileNode(ExprNode cond, StatementNode body) {
		super();
		this.cond = cond;
		this.body = body;
	}

	@Override
	public void executeVoid(TinycFrame frame) {
		try {
			while(cond.executeBoolean(frame))
				body.executeVoid(frame);
		} catch (UnexpectedResultException e) {
			Utils.panic("While condition must return boolean");
		}
	}

}
