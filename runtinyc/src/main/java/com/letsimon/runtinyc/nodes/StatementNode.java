package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.TinycFrame;

public abstract class StatementNode extends BaseNode {

	public abstract void executeVoid(TinycFrame frame);
	
}
