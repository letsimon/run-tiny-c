package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.TinycFrame;
import com.letsimon.runtinyc.Utils;
import com.oracle.truffle.api.nodes.UnexpectedResultException;

public class SumNode extends ExprNode {
	
	@Child ExprNode left;
	@Child ExprNode right;
	boolean negativeSign;
	
	public SumNode(ExprNode left2, ExprNode right2, boolean negativeSign) {
		super();
		this.left = left2;
		this.right = right2;
		this.negativeSign = negativeSign;
	}

	@Override
	public int executeInt(TinycFrame frame) {
		int sign = (negativeSign) ? -1 : 1;
		try {
			return left.executeInt(frame) + sign * right.executeInt(frame);
		} catch (UnexpectedResultException e) {
			Utils.panic("Sum operands must return int");
		}
		return 0;
	}

	@Override
	public Object execute(TinycFrame frame) {
		int sign = (negativeSign) ? -1 : 1;
		try {
			return left.executeInt(frame) + sign * right.executeInt(frame);
		} catch (UnexpectedResultException e) {
			Utils.panic("Sum operands must return int");
		}
		return 0;
	}

}
