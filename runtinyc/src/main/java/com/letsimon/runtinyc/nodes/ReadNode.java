package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.CRefString;
import com.letsimon.runtinyc.TinycFrame;

public class ReadNode extends StatementNode {

	final String id;
	
	public ReadNode(String id) {
		super();
		this.id = id;
	}

	@Override
	public void executeVoid(TinycFrame frame) {
		String val = System.console().readLine();
		CRefString str = new CRefString(val);
		frame.setString(id, str);
	}

}
