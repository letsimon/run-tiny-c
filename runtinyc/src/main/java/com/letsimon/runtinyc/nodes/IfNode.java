package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.TinycFrame;
import com.letsimon.runtinyc.Utils;
import com.oracle.truffle.api.nodes.UnexpectedResultException;

public class IfNode extends StatementNode {

	@Child ExprNode condNode;
	@Child StatementNode thenNode;
	@Child StatementNode elseNode;
	
	public IfNode(ExprNode condNode, StatementNode thenNode, StatementNode elseNode) {
		super();
		this.condNode = condNode;
		this.thenNode = thenNode;
		this.elseNode = elseNode;
	}

	@Override
	public void executeVoid(TinycFrame frame) {
		try {
			if (condNode.executeBoolean(frame)) {
				if (thenNode != null)
					thenNode.executeVoid(frame);
			} else {
				if (elseNode != null)
					elseNode.executeVoid(frame);
			}
		} catch (UnexpectedResultException e) {
			Utils.panic("If condition must return boolean");
		}
	}

}
