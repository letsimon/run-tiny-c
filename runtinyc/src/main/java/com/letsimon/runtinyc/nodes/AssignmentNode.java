package com.letsimon.runtinyc.nodes;

import com.letsimon.runtinyc.CRefString;
import com.letsimon.runtinyc.TinycFrame;
import com.letsimon.runtinyc.Utils;
import com.oracle.truffle.api.nodes.UnexpectedResultException;

public class AssignmentNode extends ExprNode {
	
	@Child ExprNode value;
	final String id;
	
	public AssignmentNode(String id, ExprNode value) {
		super();
		this.id = id;
		this.value = value;
	}

	@Override
	public int executeInt(TinycFrame frame) throws UnexpectedResultException {
		int val = value.executeInt(frame);
		frame.setInt(id, val);
        return val;
	}

	@Override
	public CRefString executeString(TinycFrame frame) throws UnexpectedResultException {
		CRefString val = value.executeString(frame);
		frame.setString(id, val);
        return val;
	}

	@Override
	public Object execute(TinycFrame frame) {
		int iVal = 0;
		CRefString sVal = null;
		try {
			iVal = value.executeInt(frame);
		} catch (UnexpectedResultException e) {
			try {
				sVal = value.executeString(frame);
			} catch (UnexpectedResultException e1) {
				Utils.panic("Assignment expression must return int or String");
			}
			frame.setString(id, sVal);
			return sVal;
		}
		frame.setInt(id, iVal);
        return iVal;
	}
}
